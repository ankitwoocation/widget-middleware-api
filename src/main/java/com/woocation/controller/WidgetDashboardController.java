package com.woocation.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.woocation.enums.CustomerType;
import com.woocation.service.DashboardService;
import com.woocation.service.response.DashboardDetails;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api")
public class WidgetDashboardController {

	/** The logger. */
	private static final Logger logger = LoggerFactory.getLogger(WidgetDashboardController.class);

	@Autowired
	private DashboardService dashboardService;

	@ApiOperation(value = "DashBoard Details", notes = "Dashboard details")
	@RequestMapping(value = "/downloadDashboard", method = RequestMethod.GET)
	public ResponseEntity<List<DashboardDetails>> downloadDashboard(@RequestParam(required = true) String clientId,
			@RequestParam(required = true) CustomerType customerType,
			@RequestParam(required = false) String entityName) {
		List<DashboardDetails> response = null;
		try {
			response = dashboardService.getDashboard(clientId, customerType, entityName);
		} catch (Exception e) {
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@ApiOperation(value = "DashBoard Details", notes = "Dashboard details")
	@RequestMapping(value = "/geoDashboard", method = RequestMethod.GET)
	public ResponseEntity<List<DashboardDetails>> geoDashboard(@RequestParam(required = true) CustomerType customerType,
			@RequestParam(required = true) String clientId) {
		List<DashboardDetails> response = null;
		try {
			response = dashboardService.geoDashboard(customerType, clientId);
		} catch (Exception e) {
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

}
