package com.woocation.dao;

import java.util.List;
import java.util.Set;

import com.woocation.dto.ActivityDTO;
import com.woocation.model.Event;

/**
 * The Interface ScriptDao.
 */
public interface EventDao {

	/**
	 * Save event.
	 *
	 * @param activityDTO
	 *            the activity DTO
	 */
	public void saveEvent(ActivityDTO activityDTO);

	/**
	 * Gets the active event.
	 *
	 * @param currentDate
	 *            the current date
	 * @return the active event
	 */
	public List<Event> getActiveEvent();

	/**
	 * Gets the events.
	 *
	 * @param userId
	 *            the user id
	 * @return the events
	 */
	public List<Event> getEvents(String userId);

	/**
	 * Gets the all entity name.
	 *
	 * @return the all entity name
	 */
	public Set<String> getAllEntityName();

	/**
	 * Gets the event by entity.
	 *
	 * @param entityName
	 *            the entity name
	 * @return the event by entity
	 */
	public List<Event> findEventByClientIdAndCustomerTypeAndEntityName(final String clientId, final String customerType,
			final String entityName);

	/**
	 * Gets the event by customer type.
	 *
	 * @param customerType
	 *            the customer type
	 * @return the event by customer type
	 */
	public List<Event> getEventByCustomerTypeAndClientId(final String customerType, final String clientId);

	/**
	 * Gets the all entity by client id and type.
	 *
	 * @param clientId
	 *            the client id
	 * @param customerType
	 *            the customer type
	 * @return the all entity by client id and type
	 */
	public Set<String> getAllEntityByClientIdAndType(final String clientId, final String customerType);

}
