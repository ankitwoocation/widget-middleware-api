package com.woocation.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.woocation.model.ArchieveEvent;

@Repository
public interface ArchiveEventRepository extends CrudRepository<ArchieveEvent, String> {

}
