package com.woocation.dao.impl;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.cassandra.core.CassandraTemplate;
import org.springframework.stereotype.Component;

import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.datastax.driver.core.querybuilder.Select;
import com.woocation.dao.EventDao;
import com.woocation.dto.ActivityDTO;
import com.woocation.model.Event;
import com.woocation.repository.EventRepository;
import com.woocation.util.EntityToDTO;
import com.woocation.util.WooDateUtils;

@Component
public class EventDaoImpl implements EventDao {

	/** The event repository. */
	@Autowired
	private EventRepository eventRepository;

	/** The template. */
	@Autowired
	private CassandraTemplate template;

	@Override
	public void saveEvent(ActivityDTO activityDTO) {
		Event event = eventRepository.findByUserId(activityDTO.getUserId());
		if (event == null) {
			event = EntityToDTO.getInstance().getEventDTO(activityDTO);
			event.setId(UUID.randomUUID());
			event.setClientId(activityDTO.getClientId());
			event.setCountryCode(activityDTO.getCountryCode());
			event.setBrowser(activityDTO.getBrowser());
			event.setCreatedDate(WooDateUtils.getCurrentDate());
			event.setUpdatedDate(WooDateUtils.getCurrentDate());
		} else {
			if (activityDTO.getAction() != null) {
				event.setAction(activityDTO.getAction().name());
			}

			if (activityDTO.getPageName() != null) {
				event.setPageName(activityDTO.getPageName());
			}

			if (activityDTO.getEntityName() != null) {
				event.setEntityName(activityDTO.getEntityName());
			}
			event.setUpdatedDate(WooDateUtils.getCurrentDate());
		}
		eventRepository.save(event);
	}

	@Override
	public List<Event> getEvents(String userId) {
		Event event = eventRepository.findByUserId(userId);
		List<Event> eventList = null;
		if (event != null) {
			Select select = QueryBuilder.select().from("events");
			select.where(QueryBuilder.eq("entityname", event.getEntityName()));
			eventList = this.template.select(select, Event.class);
		}
		return eventList;
	}

	@Override
	public List<Event> getActiveEvent() {
		Date currentDate = WooDateUtils.getActiveTimeStamp(10);
		Select select = QueryBuilder.select().from("events").allowFiltering();
		select.where(QueryBuilder.gte("updateddate", currentDate));
		List<Event> events = this.template.select(select, Event.class);
		return events;
	}

	@Override
	public Set<String> getAllEntityName() {
		List<String> allUsers = eventRepository.findAllEntityName();
		return allUsers.stream().map(item -> item).collect(Collectors.toSet());
	}

	@Override
	public List<Event> findEventByClientIdAndCustomerTypeAndEntityName(final String clientId, final String customerType,
			String entityName) {
		List<Event> entities = null;
		Select select = QueryBuilder.select().from("events").allowFiltering();
		select.where(QueryBuilder.eq("customerType", customerType)).and(QueryBuilder.eq("clientId", clientId))
				.and(QueryBuilder.eq("entityName", entityName));
		entities = this.template.select(select, Event.class);
		return entities;
	}

	@Override
	public List<Event> getEventByCustomerTypeAndClientId(String customerType, String clientId) {
		List<Event> entities = null;
		Select select = QueryBuilder.select().from("events").allowFiltering();
		select.where(QueryBuilder.eq("customerType", customerType)).and(QueryBuilder.eq("clientId", clientId));
		entities = this.template.select(select, Event.class);
		return entities;
	}

	@Override
	public Set<String> getAllEntityByClientIdAndType(String clientId, String customerType) {
		List<String> entities = null;
		Select select = QueryBuilder.select("entityname").from("events").allowFiltering();
		select.where(QueryBuilder.eq("customerType", customerType)).and(QueryBuilder.eq("clientId", clientId));
		entities = this.template.select(select, String.class);
		return entities.stream().map(item -> item).collect(Collectors.toSet());
	}

}
