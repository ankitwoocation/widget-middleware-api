package com.woocation.service;

import java.util.List;

import com.woocation.enums.CustomerType;
import com.woocation.service.response.DashboardDetails;

/**
 * The Interface DashboardService.
 */
public interface DashboardService {

	/**
	 * Gets the event details.
	 *
	 * @param clientId
	 *            the client id
	 * @param type
	 *            the type
	 * @param entityName
	 *            the entity name
	 * @return the event details
	 */
	public List<DashboardDetails> getDashboard(String clientId, CustomerType type, String entityName);

	/**
	 * Geo dashboard.
	 *
	 * @param type
	 *            the type
	 * @param clientId
	 *            the client id
	 * @return the list
	 */
	public List<DashboardDetails> geoDashboard(CustomerType type, String clientId);
}
