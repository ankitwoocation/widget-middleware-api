package com.woocation.elastic.bean;

import java.util.List;

import lombok.Data;

/**
 * The Class EsSuggest.
 */
@Data
public class EsSuggest {

	/** The field name. */
	private String fieldname;

	/** The value of field. */
	private String value;

	/** The context field name. */
	private String contextFieldname;

	/** The context value. */
	private List<String> contextValue;

	/**
	 * Default constructor.
	 * 
	 * @param fieldname
	 *            the field name
	 * @param value
	 *            the value of field
	 * @param contextFieldname
	 *            the context field name
	 * @param contextValue
	 *            the context value.
	 */
	public EsSuggest(String fieldname, String value, String contextFieldname, List<String> contextValue) {
		super();
		this.fieldname = fieldname;
		this.value = value;
		this.contextFieldname = contextFieldname;
		this.contextValue = contextValue;
	}

}
