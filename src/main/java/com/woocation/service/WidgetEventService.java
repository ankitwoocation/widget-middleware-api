package com.woocation.service;

import java.util.List;

import com.woocation.elastic.model.ESMasterEvents;
import com.woocation.elastic.model.ESUserEvent;
import com.woocation.model.Event;
import com.woocation.service.request.EventRequest;
import com.woocation.service.response.EventResponse;

// TODO: Auto-generated Javadoc
/**
 * The Interface WidgetEventService.
 */
public interface WidgetEventService {

    /**
     * Save widget event.
     *
     * @param request the request
     * @return the string
     * @throws Exception the exception
     */
    public Boolean saveWidgetEvent(EventRequest request) throws Exception;

    /**
     * Gets the features.
     *
     * @param userId the user id
     * @return the features
     * @throws Exception the exception
     */
    public EventResponse getFeatures(String userId) throws Exception;

    /**
     * Gets the master events.
     *
     * @param esClientId the es client id
     * @return the master events
     * @throws Exception the exception
     */
    public ESMasterEvents getMasterEvents(String esClientId) throws Exception;

    /**
     * Save master events.
     *
     * @param masterEvents the master events
     * @return true, if successful
     * @throws Exception the exception
     */
    public boolean saveMasterEvents(ESMasterEvents masterEvents) throws Exception;

    /**
     * Gets the event requests.
     *
     * @param activeEvent the active event
     * @return the event requests
     * @throws Exception the exception
     */
    public List<ESUserEvent> getEventRequests(List<Event> activeEvent) throws Exception;

    /**
     * Update attractions.
     *
     * @param attractions the attractions
     * @param documentId the document id
     * @return the boolean
     * @throws Exception the exception
     */
    public Boolean updateAttractions(List<String> attractions, String documentId) throws Exception;

    /**
     * Gets the active event.
     *
     * @return the active event
     */
    public List<Event> getActiveEvent();

}
