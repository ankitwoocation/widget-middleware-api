package com.woocation.dao;

import java.util.List;

import com.woocation.elastic.model.ESMasterEvents;
import com.woocation.elastic.model.ESUserEvent;

/**
 * The Interface GeoDataSearchDao.
 */
public interface WidgetEventDao {

    /**
     * Save events.
     *
     * @param userEvent the user event
     * @return the boolean
     * @throws Exception the exception
     */
    public Boolean saveEvents(ESUserEvent userEvent) throws Exception;

    /**
     * Gets the master events.
     *
     * @param id the id
     * @return the master events
     * @throws Exception the exception
     */
    public ESMasterEvents getMasterEvents(String id) throws Exception;

    /**
     * Save master events.
     *
     * @param masterEvents the master events
     * @return true, if successful
     * @throws Exception the exception
     */
    public boolean saveMasterEvents(ESMasterEvents masterEvents) throws Exception;

    /**
     * Gets the event requests.
     *
     * @param ids the ids
     * @return the event requests
     * @throws Exception the exception
     */
    public List<ESUserEvent> getEventRequests(List<String> documentIds) throws Exception;

    /**
     * Update attractions.
     *
     * @param attractions the attractions
     * @param documentId the document id
     * @return the boolean
     * @throws Exception the exception
     */
    public Boolean updateAttractions(List<String> attractions, String documentId) throws Exception;
}
