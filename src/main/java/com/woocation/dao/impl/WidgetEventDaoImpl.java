package com.woocation.dao.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.collections4.CollectionUtils;
import org.elasticsearch.script.Script;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.base.Joiner;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.woocation.dao.WidgetEventDao;
import com.woocation.elastic.core.ElasticEntityManager;
import com.woocation.elastic.model.ESMasterEvents;
import com.woocation.elastic.model.ESUserEvent;
import com.woocation.model.EventDetails;
import com.woocation.util.ObjectDeserializer;

/**
 * The Class WidgetEventDaoImpl.
 */
@Component
public class WidgetEventDaoImpl implements WidgetEventDao {

    /** The logger. */
    private static final Logger logger = LoggerFactory.getLogger(WidgetEventDaoImpl.class);

    /** The entity manager. */
    @Autowired
    private ElasticEntityManager entityManager;

    /** The cache master events. */
    private LoadingCache<String, ESMasterEvents> cacheMasterEvents = CacheBuilder.newBuilder().maximumSize(10000)
            .expireAfterAccess(5, TimeUnit.DAYS).build(new CacheLoader<String, ESMasterEvents>() {

                /**
                 * {@inheritDoc}
                 */
                @Override
                public ESMasterEvents load(String documentId) throws IOException {
                    try {
                        return getMasterEvents(documentId);
                    } catch (Exception e) {
                        logger.error("Error while getting master events for hotel id " + documentId, e);
                        return null;
                    }
                }
            });

    /**
     * Instantiates a new widget event dao impl.
     */
    public WidgetEventDaoImpl() {

    }

    /**
     * Reinit event map.
     *
     * @param events the events
     */
    private void reinitEventMap(ESUserEvent request) {
        try {
            ESMasterEvents masterEventsMappedToHotel = cacheMasterEvents.get(request.getESClientDocumentId());
            request.setEvents(getMeaningFulEvents(request.getEvents(), masterEventsMappedToHotel));
        } catch (ExecutionException e) {
            logger.error(
                    "Error while getting master events for client id from cache " + request.getESClientDocumentId(), e);
        }
    }

    /**
     * Gets the meaning ful events.
     *
     * @param userEvents the user events
     * @param mEvents the m events
     * @return the meaning ful events
     */
    private Map<String, EventDetails> getMeaningFulEvents(Map<String, EventDetails> userEvents,
            ESMasterEvents mEvents) {
        Map<String, EventDetails> meaningFulEvents = new HashMap<>();
        List<String> eventIdsToRemove = new ArrayList<>();
        mEvents.getMasterEvents().entrySet().stream().forEach(entry -> {
            if (userEvents.get(entry.getValue()) != null) {
                meaningFulEvents.put(entry.getKey(), userEvents.get(entry.getValue()));
                eventIdsToRemove.add(entry.getValue());
            }
        });
        if (CollectionUtils.isNotEmpty(eventIdsToRemove)) {
            eventIdsToRemove.stream().forEach(evId -> userEvents.remove(evId));
            userEvents.putAll(meaningFulEvents);
        }
        return userEvents;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Boolean saveEvents(ESUserEvent request) throws Exception {
        reinitEventMap(request);
        String jsonString = ObjectDeserializer.javaObjectToJson(request);
        return entityManager.save("widget-event-0.0", "Event", request.getESDocumentId(), jsonString);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Boolean updateAttractions(List<String> attractions, String documentId) throws Exception {
        StringBuilder attractionStr = new StringBuilder("['").append(Joiner.on("','").join(attractions)).append("']");
        StringBuilder scriptStr = new StringBuilder();
        scriptStr.append("ctx._source.attractions=").append(attractionStr).append(";");
        Script updateScript = new Script(scriptStr.toString());
        entityManager.updateById(ESUserEvent.INDEX_NAME, ESUserEvent.TYPE, documentId, updateScript);
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ESMasterEvents getMasterEvents(String id) throws Exception {
        ESMasterEvents masterEvents = entityManager.getById(id, ESMasterEvents.class, ESMasterEvents.INDEX_NAME,
                ESMasterEvents.DOC_TYPE);
        return masterEvents;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean saveMasterEvents(ESMasterEvents masterEvents) throws Exception {
        ESMasterEvents ev = getMasterEvents(masterEvents.getESDocumentId());
        if (ev != null) {
            entityManager.saveAndUpdateDocument(ESMasterEvents.INDEX_NAME, ESMasterEvents.DOC_TYPE,
                    masterEvents.getESDocumentId(), ObjectDeserializer.javaObjectToJson(masterEvents));
        } else {
            entityManager.save(ESMasterEvents.INDEX_NAME, ESMasterEvents.DOC_TYPE, masterEvents.getESDocumentId(),
                    ObjectDeserializer.javaObjectToJson(masterEvents));
        }
        cacheMasterEvents.put(masterEvents.getESDocumentId(), masterEvents);
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ESUserEvent> getEventRequests(List<String> documentIds) throws Exception {
        List<ESUserEvent> events = new ArrayList<>();
        for (String docId : documentIds) {
            events.add(entityManager.getById(docId, ESUserEvent.class, ESUserEvent.INDEX_NAME, ESUserEvent.TYPE));
        }
        return events;
    }

}
