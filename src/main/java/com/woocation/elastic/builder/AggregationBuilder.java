package com.woocation.elastic.builder;

import java.util.HashMap;

import org.elasticsearch.search.aggregations.AbstractAggregationBuilder;

/**
 * The Class AggregationBuilder.
 *
 * @author ankit.gupta4
 */
public class AggregationBuilder extends HashMap<String, AbstractAggregationBuilder> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor.
	 */
	public AggregationBuilder() {
		super();
	}

}
