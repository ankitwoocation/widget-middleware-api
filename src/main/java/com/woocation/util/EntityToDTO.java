package com.woocation.util;

import org.modelmapper.ModelMapper;

import com.woocation.dto.ActivityDTO;
import com.woocation.elastic.model.ESUserEvent;
import com.woocation.model.Event;
import com.woocation.service.request.EventRequest;

// TODO: Auto-generated Javadoc
/**
 * The Class EntityToDTO.
 */
public final class EntityToDTO {
    /**
     * The unique instance.
     */
    private static EntityToDTO instance;

    /**
     * The model mapper.
     */
    private ModelMapper mapper = new ModelMapper();

    /**
     * Default constructor.
     */
    private EntityToDTO() {
        initConfig();
    }

    /**
     * Initialize.
     */
    private void initConfig() {
    }

    /**
     * Gets the unique instance.
     * 
     * @return the unique instance.
     */
    public static EntityToDTO getInstance() {
        if (instance == null) {
            instance = new EntityToDTO();
        }
        return instance;
    }

    /**
     * Gets the city es DTO.
     *
     * @param activityDTO the activity DTO
     * @return the city es DTO
     */
    public Event getEventDTO(ActivityDTO activityDTO) {
        return mapper.map(activityDTO, Event.class);
    }

    /**
     * Gets the ESU ser event.
     *
     * @param eventRequest the event request
     * @return the ESU ser event
     */
    public ESUserEvent getESUSerEvent(EventRequest eventRequest) {
        return mapper.map(eventRequest, ESUserEvent.class);
    }
}
