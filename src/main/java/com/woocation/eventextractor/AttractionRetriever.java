package com.woocation.eventextractor;

import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;
import com.woocation.elastic.model.ESUserEvent;
import com.woocation.service.WidgetEventService;

public class AttractionRetriever implements Runnable {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(AttractionRetriever.class);

    /** The event service. */
    private WidgetEventService eventService;

    /** The user id. */
    private ESUserEvent eventRequest;

    /**
     * Instantiates a new attraction retriever.
     *
     * @param userId the user id
     * @param meaningFulEvents the meaning ful events
     * @param eventService the event service
     */
    public AttractionRetriever(ESUserEvent eventRequest, WidgetEventService eventService) {
        this.eventRequest = eventRequest;
        this.eventService = eventService;
    }

    /**
     * Run.
     */
    @Override
    public void run() {
        // get attractions from other services
        List<String> attractions = Lists.newArrayList("BEACH", "POOL-AREA");
        if (CollectionUtils.isNotEmpty(attractions)) {
            try {
                this.eventService.updateAttractions(attractions, eventRequest.getESDocumentId());
            } catch (Exception e) {
                LOGGER.error("Error while updating attractions for user {}", eventRequest.getESDocumentId(), e);
            }
        }
    }

}