package com.woocation.model;

import java.sql.Blob;

import org.springframework.data.cassandra.mapping.Table;

import lombok.Data;

@Table("archieve_events")
@Data
public class ArchieveEvent {
	
	/** The userId. */
	private String userId;

	/** The client id. */
	private String clientId;
	
	/** The customer type. */
	private String customerType;

	/** The entity name. */
	private String entityName;
	
	/** The statistics. */
	private Blob statistics;


}
