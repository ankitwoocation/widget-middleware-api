/**
 * 
 */
package com.woocation.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.woocation.model.Event;
import com.woocation.service.response.DashboardDetails;

/**
 * @author Ankit.Gupta
 *
 */
public class EventToDashboard {

	/** The page statistics. */
	private Map<String, Integer> statistics = new HashMap<>();

	/**
	 * Evaluate dashboard.
	 *
	 * @param entityName
	 *            the entity name
	 * @param eventList
	 *            the event list
	 * @return the dashboard details
	 */
	public DashboardDetails evaluateDashboard(String entityName, List<Event> eventList) {
		DashboardDetails dashboard = new DashboardDetails(entityName);
		for (Event event : eventList) {
			processEvent(event);
		}
		dashboard.setStatistics(statistics);
		return dashboard;
	}

	/**
	 * Evaluate dashboard.
	 *
	 * @param entityName
	 *            the entity name
	 * @param eventList
	 *            the event list
	 * @return the dashboard details
	 */
	public DashboardDetails evaluateGeoDashboard(String entityName, List<Event> eventList) {
		DashboardDetails dashboard = new DashboardDetails(entityName);
		for (Event event : eventList) {
			String countryCode = event.getCountryCode();
			Integer count = statistics.get(countryCode);
			if (count == null) {
				count = 0;
			}
			statistics.put(countryCode, count + 1);
		}
		dashboard.setStatistics(statistics);
		return dashboard;
	}

	/**
	 * Process event.
	 *
	 * @param event
	 *            the event
	 */
	private void processEvent(Event event) {
		String pageName = event.getPageName();
		Integer count = statistics.get(pageName);
		if (count == null) {
			count = 0;
		}
		statistics.put(pageName, count + 1);
	}

}
