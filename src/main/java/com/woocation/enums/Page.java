package com.woocation.enums;

public enum Page {

	LANDING,

	USERDETAIL,

	PAYMENT,

	CONFIRMATION

}
