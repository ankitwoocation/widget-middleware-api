package com.woocation.service.request;

import java.util.Map;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.woocation.enums.Action;
import com.woocation.enums.CustomerType;
import com.woocation.model.EventDetails;

import lombok.Data;

/**
 * The Class EventRequest.
 */
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class EventRequest {

	/** The user id. */
	@NotNull
	private String userId;

	/** The customer type. */
	@NotNull
	private CustomerType customerType;

	/** The client id. */
	private String clientId;

	/** The page. */
	private String page;

	/** The action. */
	private Action action;

	/** The country code. */
	private String countryCode;

	/** The browser. */
	private String browser;

	/** The event list. */
	private Map<String, EventDetails> events;

}
