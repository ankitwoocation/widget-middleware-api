package com.woocation.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.woocation.dao.EventDao;
import com.woocation.dao.WidgetEventDao;
import com.woocation.enums.CustomerType;
import com.woocation.model.Event;
import com.woocation.service.DashboardService;
import com.woocation.service.response.DashboardDetails;
import com.woocation.util.EventToDashboard;

@Service
public class DashboardServiceImpl implements DashboardService {

	@Autowired
	private WidgetEventDao widgetEventDao;

	/** The event dao. */
	@Autowired
	public EventDao eventDao;

	@Override
	public List<DashboardDetails> getDashboard(String clientId, CustomerType type, String entityName) {
		List<DashboardDetails> dashboards = new ArrayList<>();
		if (CustomerType.BOOKINGENGINE.equals(type)) {
			Set<String> entityList = eventDao.getAllEntityByClientIdAndType(clientId, type.name());
			entityList.stream().filter(entity -> entity != null).forEach(entity -> {
				dashboards.add(getDashBoardDetails(clientId, type, entity));
			});
		}

		if (CustomerType.HOTEL.equals(type)) {
			dashboards.add(getDashBoardDetails(clientId, type, entityName));
		}
		return dashboards;
	}

	private DashboardDetails getDashBoardDetails(String clientId, CustomerType type, String entityName) {
		List<Event> eventList = eventDao.findEventByClientIdAndCustomerTypeAndEntityName(clientId, type.name(),
				entityName);
		EventToDashboard eventDashBoard = new EventToDashboard();
		DashboardDetails details = eventDashBoard.evaluateDashboard(entityName, eventList);
		return details;
	}

	@Override
	public List<DashboardDetails> geoDashboard(CustomerType type, String clientId) {
		List<DashboardDetails> dashboards = new ArrayList<>();
		if (CustomerType.BOOKINGENGINE.equals(type)) {
			List<Event> eventList = eventDao.getEventByCustomerTypeAndClientId(type.name(), clientId);
			if (CollectionUtils.isNotEmpty(eventList)) {
				EventToDashboard eventDashBoard = new EventToDashboard();
				DashboardDetails details = eventDashBoard.evaluateGeoDashboard(clientId, eventList);
				dashboards.add(details);
			}
		}

		if (CustomerType.HOTEL.equals(type)) {
			List<Event> eventList = eventDao.getEventByCustomerTypeAndClientId(clientId, type.name());
			if (CollectionUtils.isNotEmpty(eventList)) {
				EventToDashboard eventDashBoard = new EventToDashboard();
				DashboardDetails details = eventDashBoard.evaluateGeoDashboard(clientId, eventList);
				dashboards.add(details);
			}
		}
		return dashboards;
	}
}
