package com.woocation.elastic.model;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.woocation.enums.CustomerType;

// TODO: Auto-generated Javadoc
/**
 * The Class MasterEvents.
 */
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ESMasterEvents {

    /** The Constant INDEX_NAME. */
    public static final String INDEX_NAME = "master_events";

    /** The Constant DOC_TYPE. */
    public static final String DOC_TYPE = "doc_type";

    /** The hotel id. */
    private String clientId;

    /** The client type. */
    private CustomerType clientType;

    /** The master events. */
    private Map<String, String> masterEvents;

    /**
     * Instantiates a new master events.
     */
    public ESMasterEvents() {
        masterEvents = new HashMap<>();
    }

    /**
     * Instantiates a new master events.
     *
     * @param id the id
     */
    public ESMasterEvents(String id) {
        masterEvents = new HashMap<>();
        this.clientId = id;
    }

    /**
     * Getter for property masterEvents.
     * 
     * @return Value of property masterEvents.
     */
    public Map<String, String> getMasterEvents() {
        return masterEvents;
    }

    /**
     * Setter for property masterEvents.
     * 
     * @param masterEvents New value of property masterEvents.
     */
    public void setMasterEvents(Map<String, String> masterEvents) {
        this.masterEvents = masterEvents;
    }

    /**
     * Getter for property clientId.
     * 
     * @return Value of property clientId.
     */
    public String getClientId() {
        return clientId;
    }

    /**
     * Setter for property clientId.
     * 
     * @param clientId New value of property clientId.
     */
    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    /**
     * Getter for property clientType.
     * 
     * @return Value of property clientType.
     */
    public CustomerType getClientType() {
        return clientType;
    }

    /**
     * Setter for property clientType.
     * 
     * @param clientType New value of property clientType.
     */
    public void setClientType(CustomerType clientType) {
        this.clientType = clientType;
    }

    /**
     * Gets the ES document id.
     *
     * @return the ES document id
     */
    public String getESDocumentId() {
        return this.clientId.concat("_").concat(this.clientType.toString());
    }

}
