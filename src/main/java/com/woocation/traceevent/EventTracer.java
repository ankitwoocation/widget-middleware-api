package com.woocation.traceevent;

import static net.logstash.logback.marker.Markers.appendEntries;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.woocation.service.request.EventRequest;

@Component
@Scope(BeanDefinition.SCOPE_SINGLETON)
public class EventTracer {

    /** The monitor-logger. */
    private static final Logger TRACER = LoggerFactory.getLogger("EVENTTRACER");

    /** The Constant USERID. */
    private static final String USERID = "userId";

    /** The Constant events. */
    private static final String EVENTS = "events";

    /**
     * Trace.
     *
     * @param request the request
     */
    public static void trace(EventRequest request) {
        Map<String, Object> traceMap = new HashMap<>();
        traceMap.put(USERID, request.getUserId());
        traceMap.put(EVENTS, request.getEvents());

        TRACER.info(appendEntries(traceMap), "Event Data Saved");
    }
}
