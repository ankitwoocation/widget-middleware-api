/**
 * 
 */
package com.woocation.util;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;

/**
 * The Class DayUtils.
 *
 * @author ankit.gupta
 */
public class DayUtils {

	/**
	 * Gets the day.
	 *
	 * @param date
	 *            the date
	 * @return the day
	 */
	public static DayOfWeek getDay(final String date) {
		try {
			LocalDate localDate = WooDateUtils.getDate(date);
			return localDate.getDayOfWeek();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Gets the working days.
	 *
	 * @param workingText
	 *            the working text
	 * @return the working days
	 */
	public static ImmutablePair<DayOfWeek, DayOfWeek> getWorkingDays(final String workingText) {
		String[] days = workingText.split("-");
		return new ImmutablePair<DayOfWeek, DayOfWeek>(DayOfWeek.valueOf(days[0]), DayOfWeek.valueOf(days[1]));
	}

	/**
	 * Gets the non working days.
	 *
	 * @param workingText
	 *            the working text
	 * @return the non working days
	 */
	public static List<DayOfWeek> getNonWorkingDays(final String workingText) {
		String[] days = workingText.split("-");
		DayOfWeek startDay = DayOfWeek.valueOf(days[0]);
		DayOfWeek lastDay = DayOfWeek.valueOf(days[1]);

		List<DayOfWeek> nonWorkingDays = new ArrayList<>();
		DayOfWeek tempDay = lastDay;
		while(!tempDay.equals(startDay)){
			tempDay = tempDay.plus(1);
			if(!tempDay.equals(startDay)){
				nonWorkingDays.add(tempDay);
			}
		}
		return nonWorkingDays;
	}
	

	public static void main(String[] args) {
		getNonWorkingDays("MONDAY-FRIDAY");
	}

}
