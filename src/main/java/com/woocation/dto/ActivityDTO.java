/**
 * 
 */
package com.woocation.dto;

import com.woocation.enums.Action;
import com.woocation.enums.CustomerType;

import lombok.Data;

/**
 * The Class ActivityDTO.
 *
 * @author ankit.gupta
 */
@Data
public class ActivityDTO {

	/** The user id. */
	private String userId;

	/** The client id. */
	private String clientId;

	/** The country code. */
	private String countryCode;

	/** The browser. */
	private String browser;

	/** The customer type. */
	private CustomerType customerType;

	/** The entity name. */
	private String entityName;

	/** The page name. */
	private String pageName;

	/** The action. */
	private Action action;

}
