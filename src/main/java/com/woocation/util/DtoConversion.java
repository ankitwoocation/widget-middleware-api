/**
 * 
 */
package com.woocation.util;

import java.util.Map;

import com.woocation.dto.ActivityDTO;
import com.woocation.model.EventDetails;
import com.woocation.service.request.EventRequest;

/**
 * The Class DtoConversion.
 *
 * @author admin
 */
public class DtoConversion {

	/**
	 * Gets the activity DTO.
	 *
	 * @param request
	 *            the request
	 * @return the activity DTO
	 */
	public static ActivityDTO getActivityDTO(EventRequest request) {
		ActivityDTO activityDTO = new ActivityDTO();
		activityDTO.setUserId(request.getUserId());
		activityDTO.setClientId(request.getClientId());
		activityDTO.setCustomerType(request.getCustomerType());
		activityDTO.setBrowser(request.getBrowser());
		activityDTO.setCountryCode(request.getCountryCode());
		activityDTO.setAction(request.getAction());
		activityDTO.setPageName(request.getPage());
		Map<String, EventDetails> eventMap = request.getEvents();
		EventDetails details = eventMap.get("hotelName");
		if (details != null) {
			activityDTO.setEntityName(details.getValue());
		}
		return activityDTO;
	}
}
