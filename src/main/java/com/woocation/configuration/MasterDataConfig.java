package com.woocation.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

// TODO: Auto-generated Javadoc
/**
 * The Class GeoConfig.
 */
@Configuration
public class MasterDataConfig {

	/** The index name. */
	@Value("${server.port}")
	private int serverPort;
	
	/** The index name. */
	@Value("${http.server.port}")
	private int httpServerPort;
	
	/** The index name. */
	@Value("${elastic.event.index.name}")
	private String indexName;
	
	/** The doc type. */
	@Value("${elastic.event.docType}")
	private String docType;

	/** The index name. */
	@Value("${middleware.application.injection}")
	private String appInjection;

	/**
	 * Gets the index name.
	 *
	 * @return the index name
	 */
	public String getIndexName() {
		return indexName;
	}

	/**
	 * Sets the index name.
	 *
	 * @param indexName the new index name
	 */
	public void setIndexName(String indexName) {
		this.indexName = indexName;
	}

	/**
	 * Gets the doc type.
	 *
	 * @return the doc type
	 */
	public String getDocType() {
		return docType;
	}

	/**
	 * Sets the doc type.
	 *
	 * @param docType the new doc type
	 */
	public void setDocType(String docType) {
		this.docType = docType;
	}

	/**
	 * Gets the app injection.
	 *
	 * @return the app injection
	 */
	public String getAppInjection() {
		return appInjection;
	}

	/**
	 * Sets the app injection.
	 *
	 * @param appInjection the new app injection
	 */
	public void setAppInjection(String appInjection) {
		this.appInjection = appInjection;
	}

	/**
	 * Gets the server port.
	 *
	 * @return the server port
	 */
	public int getServerPort() {
		return serverPort;
	}

	/**
	 * Sets the server port.
	 *
	 * @param serverPort the new server port
	 */
	public void setServerPort(int serverPort) {
		this.serverPort = serverPort;
	}

	/**
	 * Gets the http server port.
	 *
	 * @return the http server port
	 */
	public int getHttpServerPort() {
		return httpServerPort;
	}

	/**
	 * Sets the http server port.
	 *
	 * @param httpServerPort the new http server port
	 */
	public void setHttpServerPort(int httpServerPort) {
		this.httpServerPort = httpServerPort;
	};
}
