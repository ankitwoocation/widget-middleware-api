package com.woocation.elastic.model;

import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.woocation.enums.CustomerType;
import com.woocation.model.EventDetails;

import lombok.Data;

/**
 * The Class UserEvent.
 */
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class ESUserEvent {

	/** The Constant INDEX_NAME. */
	public static final String INDEX_NAME = "widget-event-0.0";

	/** The Constant TYPE. */
	public static final String TYPE = "UserEvent";

	/** The Constant USERID_FIELD_REF. */
	public static final String USERID_FIELD_REF = "userId.raw";

	/** The user id. */
	@NotNull
	private String userId;

	/** The client id. */
	private String clientId;

	/** The customer type. */
	@NotNull
	private CustomerType customerType;

	/** The event list. */
	private Map<String, EventDetails> events;

	/** The attractions. */
	private List<String> attractions;

	/**
	 * Gets the ES document id.
	 *
	 * @return the ES document id
	 */
	public String getESDocumentId() {
		return this.userId.concat("_").concat(this.clientId.toString()).concat("_")
				.concat(this.customerType.toString());
	}

	/**
	 * Gets the ES client document id.
	 *
	 * @return the ES client document id
	 */
	public String getESClientDocumentId() {
		return this.clientId.toString().concat("_").concat(this.customerType.toString());
	}
}
