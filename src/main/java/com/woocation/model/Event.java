package com.woocation.model;

import java.util.Date;
import java.util.UUID;

import org.springframework.data.cassandra.mapping.PrimaryKey;
import org.springframework.data.cassandra.mapping.Table;

import lombok.Data;

/**
 * The Class Script.
 */
@Table("events")
@Data
public class Event {

	/** The id. */
	@PrimaryKey
	private UUID id;

	/** The userId. */
	private String userId;

	/** The client id. */
	private String clientId;

	/** The country code. */
	private String countryCode;

	/** The browser. */
	private String browser;

	/** The customer type. */
	private String customerType;

	/** The entity name. */
	private String entityName;

	/** The action. */
	private String action;

	/** The page name. */
	private String pageName;

	/** The create at. */
	private Date createdDate;

	/** The updated at. */
	private Date updatedDate;
}
