package com.woocation.service.response;

import java.util.Map;

import lombok.Data;

/**
 * The Class DashboardResponse.
 */
@Data
public class DashboardDetails {

	/** The entity name. */
	private String entityName;

	/** The page statistics. */
	private Map<String, Integer> statistics;

	/**
	 * Instantiates a new dashboard details.
	 */
	public DashboardDetails() {
	}

	/**
	 * Instantiates a new dashboard details.
	 *
	 * @param entityName
	 *            the entity name
	 */
	public DashboardDetails(final String entityName) {
		this.entityName = entityName;
	}
}
