package com.woocation.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.cassandra.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.woocation.model.Event;

/**
 * The Interface ActiveEventRepository.
 */
@Repository
public interface EventRepository extends CrudRepository<Event, String> {

	/**
	 * Find by user id.
	 *
	 * @param userId
	 *            the user id
	 * @return the active event
	 */
	Event findByUserId(String userId);

	/**
	 * Find by date.
	 *
	 * @param currentDate
	 *            the current date
	 * @return the list
	 */
	@Query(value = "select * from ActiveEvent where updatedDate < (?1) ALLOW FILTERING")
	List<Event> findByDate(Date currentDate);

	/**
	 * Find all entity name.
	 *
	 * @return the list
	 */
	@Query(value = "select entityName from events")
	List<String> findAllEntityName();

	/**
	 * Find all entity by client id and type.
	 *
	 * @param customerType
	 *            the customer type
	 * @param clientId
	 *            the client id
	 * @return the list
	 */
	@Query(value = "select entityName from events where customerType = (?1) and clientId = (?2) ALLOW FILTERING")
	List<String> findEntityByCustomerTypeAndClientId(String customerType, String clientId);

	/**
	 * Find by entity name.
	 *
	 * @param entityName
	 *            the entity name
	 * @return the list
	 */
	List<Event> findByEntityName(String entityName);

	/**
	 * Find by entity name.
	 *
	 * @param entityName
	 *            the entity name
	 * @return the list
	 */
	List<Event> findByCustomerTypeAndClientIdAndEntityName(String customerType, String clientId, String entityName);

	/**
	 * Find by customer type and client id.
	 *
	 * @param customerType
	 *            the customer type
	 * @param clientId
	 *            the client id
	 * @return the list
	 */
	@Query(value = "select * from events where customerType = (?1) and clientId = (?2) ALLOW FILTERING")
	List<Event> findByCustomerTypeAndClientId(String customerType, String clientId);

}
