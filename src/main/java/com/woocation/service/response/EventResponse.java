package com.woocation.service.response;

import java.util.List;

import lombok.Data;

/**
 * The Class EventResponse.
 */
@Data
public class EventResponse {

	/** The user id. */
	private String userId;

	/** The user events. */
	private List<String> userEvents;

	/** The active events. */
	private List<String> activeEvents;

}
