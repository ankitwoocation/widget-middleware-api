package com.woocation.eventextractor;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import com.woocation.elastic.model.ESUserEvent;
import com.woocation.model.Event;
import com.woocation.service.WidgetEventService;

/**
 * The Class EventExtractor.
 */
@Configuration
@EnableScheduling
public class EventExtractor {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(EventExtractor.class);

    /** The event service. */
    @Autowired
    private WidgetEventService eventService;

    /** The thread pool. */
    private ExecutorService threadPool;

    /** The index name. */
    @Value("${job.extractor.scheduler.enabled}")
    private boolean schedulerIsEnabled;

    /**
     * Instantiates a new event extractor.
     */
    public EventExtractor() {
        threadPool = Executors.newCachedThreadPool();
    }

    /**
     * Extract events.
     */
    @Scheduled(initialDelay = 1000, fixedDelayString = "${job.schedular.duration}")
    public void extractEvents() {
        if (schedulerIsEnabled) {
            try {
                List<Event> activeEvent = eventService.getActiveEvent();
                if (CollectionUtils.isNotEmpty(activeEvent)) {
                    List<ESUserEvent> eventRequests = eventService.getEventRequests(activeEvent);
                    for (ESUserEvent eventReq : eventRequests) {
                        AttractionRetriever attractionRetriever = new AttractionRetriever(eventReq, eventService);
                        threadPool.submit(attractionRetriever);

                        // get attractions from service
                        // save/ update attractions in cassendra ?
                    }
                }
            } catch (Exception ex) {
                LOGGER.error(ex.getMessage(), ex);
            }
        }
    }
}
