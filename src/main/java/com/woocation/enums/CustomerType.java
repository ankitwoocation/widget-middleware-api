package com.woocation.enums;

/**
 * The Enum CustomerType.
 */
public enum CustomerType {

	/** The bookingengine. */
	BOOKINGENGINE,

	/** The hotel. */
	HOTEL,

	/** The ota. */
	OTA

}
