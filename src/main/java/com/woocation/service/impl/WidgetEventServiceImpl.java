package com.woocation.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.woocation.dao.EventDao;
import com.woocation.dao.WidgetEventDao;
import com.woocation.elastic.model.ESMasterEvents;
import com.woocation.elastic.model.ESUserEvent;
import com.woocation.model.Event;
import com.woocation.service.WidgetEventService;
import com.woocation.service.request.EventRequest;
import com.woocation.service.response.EventResponse;
import com.woocation.util.DtoConversion;
import com.woocation.util.EntityToDTO;

/**
 * The Class GeoDataUploadServiceImpl.
 */
@Service
public class WidgetEventServiceImpl implements WidgetEventService {

    /** The logger. */
    private static final Logger logger = LoggerFactory.getLogger(WidgetEventServiceImpl.class);

    @Autowired
    private WidgetEventDao widgetDao;

    /** The active event dao. */
    @Autowired
    private EventDao eventDao;

    /**
     * {@inheritDoc}
     */
    @Override
    public Boolean saveWidgetEvent(EventRequest request) throws Exception {
        try {
            eventDao.saveEvent(DtoConversion.getActivityDTO(request));
            widgetDao.saveEvents(EntityToDTO.getInstance().getESUSerEvent(request));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public EventResponse getFeatures(String userId) throws Exception {
        EventResponse response = new EventResponse();
        response.setUserId(userId);
        response.setActiveEvents(getActiveFeature(userId));
        response.setUserEvents(getUserFeature(userId));
        return response;
    }

    /**
     * Gets the feature.
     *
     * @param userId the user id
     * @return the feature
     */
    private List<String> getUserFeature(String userId) {
        List<String> activeEvents = new ArrayList<>();
        return activeEvents;
    }

    /**
     * Gets the active feature.
     *
     * @param userId the user id
     * @return the active feature
     */
    private List<String> getActiveFeature(String userId) {
        List<String> activeEvents = new ArrayList<>();
        List<Event> eventList = eventDao.getActiveEvent();
        if (CollectionUtils.isNotEmpty(eventList)) {
            activeEvents.add(eventList.size() + " members currently active");
        }

        if (userId != null) {
            List<Event> entityEventList = eventDao.getEvents(userId);
            if (CollectionUtils.isNotEmpty(entityEventList)) {
                activeEvents.add(entityEventList.size() + " members looking for this Hotel");
            }
        }
        return activeEvents;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ESMasterEvents getMasterEvents(String esClientId) throws Exception {
        ESMasterEvents masterEvents = widgetDao.getMasterEvents(esClientId);
        if (masterEvents == null) {
            masterEvents = new ESMasterEvents(esClientId);
        }
        return masterEvents;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean saveMasterEvents(ESMasterEvents masterEvents) throws Exception {
        return widgetDao.saveMasterEvents(masterEvents);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ESUserEvent> getEventRequests(List<Event> activeEvent) throws Exception {
        List<String> eventIds = new ArrayList<>();
        activeEvent.stream().forEach(activeEv -> eventIds
                .add(activeEv.getUserId() + "_" + activeEv.getEntityName() + "_" + activeEv.getCustomerType()));
        return widgetDao.getEventRequests(eventIds);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Boolean updateAttractions(List<String> attractions, String documentId) throws Exception {
        return widgetDao.updateAttractions(attractions, documentId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Event> getActiveEvent() {
        return eventDao.getActiveEvent();
    }

}
