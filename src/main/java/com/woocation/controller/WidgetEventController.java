package com.woocation.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.woocation.configuration.MasterDataConfig;
import com.woocation.elastic.model.ESMasterEvents;
import com.woocation.service.WidgetEventService;
import com.woocation.service.request.EventRequest;
import com.woocation.service.response.EventResponse;
import com.woocation.traceevent.EventTracer;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api")
public class WidgetEventController {

    /** The logger. */
    private static final Logger logger = LoggerFactory.getLogger(WidgetEventController.class);

    @Autowired
    private WidgetEventService widgetEventService;

    @Autowired
    private MasterDataConfig config;

    /**
     * Search query time.
     *
     * @param flightQuery the flight query
     * @return the response entity
     */
    @ApiOperation(value = "Widget Event save", notes = "Widget Event save")
    @RequestMapping(value = "/saveEvent", method = RequestMethod.POST)
    public ResponseEntity<String> saveEvent(@RequestBody EventRequest eventRequest) {
        try {
            if (config.getAppInjection().equalsIgnoreCase("Logstash")) {
                EventTracer.trace(eventRequest);
            } else {
                widgetEventService.saveWidgetEvent(eventRequest);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("", HttpStatus.OK);
    }

    @ApiOperation(value = "Widget Event save", notes = "Widget Event save")
    @RequestMapping(value = "/getFeatures", method = RequestMethod.GET)
    public ResponseEntity<EventResponse> saveEvent(@RequestParam(required = false) String userId) {
        EventResponse response = null;
        try {
            response = widgetEventService.getFeatures(userId);
        } catch (Exception e) {
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "Check Options Type API", notes = "Check Options Type API")
    @RequestMapping(value = "/checkOptions", method = RequestMethod.OPTIONS)
    public ResponseEntity<String> checkOptions() {
        String message = "Ok";
        try {
            return new ResponseEntity<>(message, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Gets the master events.
     *
     * @param clientId the client id
     * @param clientType the client type
     * @return the master events
     */
    @ApiOperation(value = "Master Events retrieval", notes = "Master Events retrieval")
    @RequestMapping(value = "/getMasterEvents", method = RequestMethod.GET)
    public ResponseEntity<ESMasterEvents> getMasterEvents(@RequestParam String clientId,
            @RequestParam String clientType) {
        ESMasterEvents masterEvents = null;
        try {
            masterEvents = widgetEventService.getMasterEvents(clientId + "_" + clientType);
        } catch (Exception e) {
            return new ResponseEntity<>(masterEvents, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(masterEvents, HttpStatus.OK);
    }

    /**
     * Gets the master events.
     *
     * @param masterEvents the master events
     * @return the master events
     */
    @ApiOperation(value = "Master Events save", notes = "Master Events save")
    @RequestMapping(value = "/saveMasterEvents", method = RequestMethod.POST)
    public ResponseEntity<Boolean> saveMasterEvents(@RequestBody ESMasterEvents masterEvents) {
        try {
            widgetEventService.saveMasterEvents(masterEvents);
        } catch (Exception e) {
            return new ResponseEntity<>(false, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(true, HttpStatus.OK);
    }
}
