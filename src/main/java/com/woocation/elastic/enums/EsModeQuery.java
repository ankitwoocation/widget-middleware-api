package com.woocation.elastic.enums;

// TODO: Auto-generated Javadoc
/**
 * The Enum EsModeQuery.
 * 
 * @author ankit.gupta4
 */
public enum EsModeQuery {

    /** The match. */
    MATCH,

    /** The range. */
    RANGE,

    /** The geo distance. */
    GEO_DISTANCE,

    /** The term. */
    TERM,

    /** The terms. */
    TERMS,

    /** The query string query. */
    QUERY_STRING_QUERY,

    /** The function score. */
    FUNCTION_SCORE;
}